# Products API
Based on Symfony 6, this project provides a micro-service, which can be used to query the Ebay Finding API.

#### Why Symfony
Symfony is one of the original ones out there and provide vast possibilities of customizations and of scaling the application. No matter the complexity of the project, Symfony can pretty much handle it. There is no one-size-fits-all solution in the world in real sense, but Symfony comes very close.

### Demo
Basic URI (opens in the same tab): https://compado.umairsarwar.com/index.php/products

Full URI (opens in the same tab): https://compado.umairsarwar.com/index.php/products?keywords=batman&price_max=560&price_min=2&sorting=PricePlusShippingHighest

If you have Postman installed, then the testing will be like a breeze.

### Parameters
| Name | Required | Description |
| ---      | ---      |  ---      |
| `keywords`   | Yes | This parameter can contain any type of value |
| `price_min`   | No | Can only contain digits and must be greater than zero |
| `price_max`   | No | Can only contain digits and must be greater than zero |
| `sorting`   | No | Can only contain either `PricePlusShippingLowest` or `PricePlusShippingHighest` |

### Composer Packages
#### Installed by Symfony by default
```
symfony/console
symfony/dotenv
symfony/flex
symfony/framework-bundle
symfony/runtime
symfony/yaml
```

#### Installed by _yours truly_
```
guzzlehttp/guzzle // An HTTP client to call Ebay Finding API, guzzlehttp is constantly maintained and has very good reputation on both GitHub and Packagist  
symfony/validator // So that I could validate the API parameters before using them. This package already belongs to Symfony eco-system, so works very smoothly with it
```

### Bonus
1. Sometimes requesting data for the keyword takes a long time (> 5 seconds), what can be done to make it faster?
    * Cache the response for certain params for a certain period of time, after which it automatically expires and will generate anew
    * It will:
      * lower the response time to microseconds
      * save the server a lot of processing
      * save the API call limit as well

2. What are downsides of this solution?
    * Cache:
      * The response is not real-time
      * Users may get frustrate after trying multiple times in a row

4. We’d like to add as many product feeds as possible, how would you structure a service with this requirement?
    * All the product feeds implement a common `interface`
    * In this way they have the same contract and `interface` can be used as a DI
    * If we want to search in multiple feeds:
      * Search service calls the respective Clients one-by-one in `async` sub-processes
      * As soon as the sub-process is finished, the result is stored in a common array

### What is not yet implemented
1. Authorization
2. Pagination of the results
3. Validation of the response
4. Logging mechanism
5. Specialized Exception classes for the errors
