<?php

namespace App\Service;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;

class EbayClient implements ProductsFeedClient
{
    use LoggerAwareTrait;

    protected string $feedUrl;

    protected string $appId;

    public function __construct(string $feedUrl, string $appId)
    {
        $this->feedUrl = $feedUrl;
        $this->appId = $appId;
        $this->logger = new NullLogger();
    }

    /**
     * @param string $keywords
     * @param array $filters
     * @param string $sorting
     * @return array
     */
    public function getProductsByKeywords(string $keywords, string $sorting, array $filters = []): array
    {
        $options = [
            'query' => [
                'OPERATION-NAME' => 'findItemsByKeywords',
                'SECURITY-APPNAME' => $this->appId,
                'RESPONSE-DATA-FORMAT' => 'json',
                'keywords' => $keywords,
                'sortOrder' => $sorting
            ]
        ];

        $itemFilter = [];
        $filterCounter = 0;
        foreach ($filters as $filter => $value) {
            switch ($filter) {
                case 'price_max':
                    $itemFilter["itemFilter({$filterCounter}).name"] = 'MaxPrice';
                    break;
                case 'price_min':
                    $itemFilter["itemFilter({$filterCounter}).name"] = 'MinPrice';
                    break;
            }

            $itemFilter["itemFilter({$filterCounter}).value"] = $value;
            $filterCounter++;
        }

        $options['query'] = array_merge($options['query'], $itemFilter);

        try {
            $client = new Client();
            $response = $client->request('GET', $this->feedUrl, $options);
            $this->handleResponse($response);
        } catch (GuzzleException $exception) {
            // todo: log the GuzzleException error
            $this->logger->error($exception->getMessage());
            return ['error' => $exception->getMessage()];
        } catch (Exception $exception) {
            // todo: log the error
            $this->logger->error($exception->getMessage());
            return ['error' => $exception->getMessage()];
        }

        return $this->parseResponse($response);
    }

    /**
     * @throws Exception
     */
    public function handleResponse(ResponseInterface $response): void
    {
        if ($response->getStatusCode() < 200 || $response->getStatusCode() >= 300) {
            $errorMessage = $response->getReasonPhrase();
            throw new Exception($errorMessage, 1642639631);
        }

        $responseContentsJson = $response->getBody()->getContents();
        $responseContents = json_decode($responseContentsJson, true);
        if (!$responseContents) {
            throw new Exception('Response could not be parsed as valid JSON', 1642639638);
        }

        if (isset($responseContents['errorMessage'])) {
            throw new Exception($responseContents['errorMessage']['error'][0]['message'], 1642639645);
        }

        $findItemsByKeywordsResponse = array_shift($responseContents);
        $findItemsByKeywordsResponseInner = array_shift($findItemsByKeywordsResponse);

        if (isset($findItemsByKeywordsResponseInner['ack']) &&
            ($findItemsByKeywordsResponseInner['ack'][0] === 'Failure' ||
                $findItemsByKeywordsResponseInner['ack'][0] === 'PartialFailure')
        ) {
            $errorMessage = $findItemsByKeywordsResponseInner['errorMessage'][0]['error'][0]['message'][0];
            $errorMessage .= ' Parameter: ' . $findItemsByKeywordsResponseInner['errorMessage'][0]['error'][0]['parameter'][0];
            throw new Exception($errorMessage, 1642639656);
        }
    }

    public function parseResponse(ResponseInterface $response): array
    {
        $response->getBody()->rewind();
        $responseContents = json_decode($response->getBody()->getContents(), true);
        $findItemsByKeywordsResponse = array_shift($responseContents);
        $findItemsByKeywordsResponseInner = array_shift($findItemsByKeywordsResponse);

        $searchResult = array_shift($findItemsByKeywordsResponseInner['searchResult']);

        $return = [];
        $return['meta']['total_results'] = $searchResult['@count'];
        $return['results'] = [];

        if ($searchResult['@count'] == 0) {
            return $return;
        }

        $items = $searchResult['item'];

        $products = [];
        foreach ($items as $item) {
            $price = '';
            if (isset($item['sellingStatus'][0]['currentPrice'][0]['__value__'])) {
                $price = $item['sellingStatus'][0]['currentPrice'][0]['__value__'];
            }

            $shippingPrice = $price;
            if (isset($item['shippingInfo'][0]['shippingServiceCost'][0]['__value__'])) {
                $shippingPrice = $price + $item['shippingInfo'][0]['shippingServiceCost'][0]['__value__'];
            }

            $currency = '';
            if (isset($item['sellingStatus'][0]['currentPrice'][0]['@currencyId'])) {
                $currency = $item['sellingStatus'][0]['currentPrice'][0]['@currencyId'];
            }

            $endTime = '';
            if (isset($item['listingInfo'][0]['endTime'][0])) {
                $endTime = $item['listingInfo'][0]['endTime'][0];
            }

            $product = [
                'provider' => 'ebay',
                'item_id' => $item['itemId'][0],
                'click_out_link' => $item['viewItemURL'][0],
                'main_photo_url' => $item['galleryURL'][0],
                'price' => $price,
                'price_currency' => $currency,
                'shipping_price' => $shippingPrice,
                'title' => $item['title'][0],
                'valid_until' => $endTime,
                'description' => '',
                'brand' => ''
            ];

            $products[] = $product;
        }

        $return['results'] = $products;

        return $return;
    }
}