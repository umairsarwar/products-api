<?php

namespace App\Service;

class SearchService
{
    protected EbayClient $productsFeedClient;

    public function __construct(EbayClient $ebayClient)
    {
        $this->productsFeedClient = $ebayClient;
    }

    /**
     * @param array $parameters
     * @return array
     */
    public function search(array $parameters): array
    {
        $keywords = urlencode($parameters['keywords']);

        $filters = [];

        if (!empty($parameters['price_min'])) {
            $filters['price_min'] = $parameters['price_min'];
        }

        if (!empty($parameters['price_max'])) {
            $filters['price_max'] = $parameters['price_max'];
        }

        $sorting = 'PricePlusShippingLowest';
        if (!empty($parameters['sorting'])) {
            $sorting = $parameters['sorting'];
        }

        return $this->productsFeedClient->getProductsByKeywords($keywords, $sorting, $filters);
    }
}