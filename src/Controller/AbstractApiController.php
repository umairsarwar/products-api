<?php

namespace App\Controller;

use App\Service\SearchService;
use App\Service\ProductsFeedClient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Exception\ValidationFailedException;
use Symfony\Component\Validator\Validation;

abstract class AbstractApiController extends AbstractController
{
    protected SearchService $searchService;

    public function __construct(SearchService $productFeed)
    {
        $this->searchService = $productFeed;
    }

    /**
     * @param Request $request
     */
    protected function authorize(Request $request)
    {
        //todo: write authorization logic
    }

    /**
     * @param Request $request
     * @return array
     */
    protected function getPayloadFromRequest(Request $request): array
    {
        $payload = [];
        $queryString = $request->getQueryString();
        parse_str($queryString, $payload);

        return $payload;
    }

    protected function getConstraints(): array
    {
        return [
            'keywords' => [
                new Constraints\Required(),
                new Constraints\NotBlank()
            ],
            'price_min' => [
                new Constraints\Optional([
                    new Constraints\GreaterThan(0),
                    new Constraints\Type('digit')
                ])
            ],
            'price_max' => [
                new Constraints\Optional([
                    new Constraints\GreaterThan(0),
                    new Constraints\Type('digit')
                ])
            ],
            'sorting' => [
                new Constraints\Optional([
                    new Constraints\Choice([
                        'PricePlusShippingHighest',
                        'PricePlusShippingLowest'
                    ])
                ])
            ]
        ];
    }

    /**
     * @param array $payload
     * @return array
     */
    protected function getValidationErrors(array $payload = []): array
    {
        $constraints = $this->getConstraints();
        $collection = new Constraints\Collection($constraints);

        $validator = Validation::createValidator();
        $violations = $validator->validate($payload, $collection);

        $errors = [];
        /** @var ConstraintViolation $violation */
        foreach ($violations as $violation) {
            $errors[$violation->getPropertyPath()] = $violation->getMessage();
        }

        return $errors;
    }

    /**
     * @param string|array $message
     * @return JsonResponse
     */
    protected function getJsonErrorResponse(string|array $message = ''): JsonResponse
    {
        $errorResponse = [
            'errors' => $message
        ];

        return new JsonResponse($errorResponse, Response::HTTP_BAD_REQUEST);
    }
}