<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class SearchController extends AbstractApiController
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function products(Request $request): JsonResponse
    {
        // Step 1: Authorization
        $this->authorize($request);

        $payload = $this->getPayloadFromRequest($request);

        // Step 2: Validation
        $errors = $this->getValidationErrors($payload);
        if (count($errors) > 0) {
            return $this->getJsonErrorResponse($errors);
        }

        // Step 3: using the search service to read ebay product feed
        $result = $this->searchService->search($payload);
        if (isset($result['error'])) {
            return $this->getJsonErrorResponse($result['error']);
        }

        return new JsonResponse($result);
    }
}